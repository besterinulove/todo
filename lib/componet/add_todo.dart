import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo/provider/priority_provider.dart';
import 'package:todo/provider/todo_provider.dart';

enum Priority {
  none(''),
  low('!'),
  medium('!!'),
  height('!!!');

  final String symbol;

  const Priority(this.symbol);
}

const _priority = [
  ButtonSegment(value: Priority.none, label: Text('None')),
  ButtonSegment(value: Priority.low, label: Text('Low')),
  ButtonSegment(value: Priority.medium, label: Text('Medium')),
  ButtonSegment(value: Priority.height, label: Text('Height')),
];

void showAddToDoBottomSheet(BuildContext context) {
  showModalBottomSheet(context: context, isScrollControlled: true, builder: (_) => const AddToDo());
}

class AddToDo extends ConsumerStatefulWidget {
  const AddToDo({super.key});

  @override
  ConsumerState<AddToDo> createState() => _AddToDoState();
}

class _AddToDoState extends ConsumerState<AddToDo> {
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final priority=ref.watch(priorityProvider);
    return Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.viewInsetsOf(context).bottom),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.close)),
                IconButton(
                    onPressed: () {
                      if (_controller.text.isEmpty) return;
                      ref.read(todoListProvider.notifier).add(priority, _controller.text);
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.save))
              ],
            ),
            TextField(
              controller: _controller,
              decoration: const InputDecoration(
                label: Text('Content'),
              ),
            ),
            const SizedBox(height: 28),
            const Text('Important'),
            const SizedBox(height: 16),
            SizedBox(
              width: double.infinity,
              child: SegmentedButton<Priority>(
                  showSelectedIcon: false,
                  segments: _priority,
                  selected: {priority},
                  onSelectionChanged: (Set<Priority> newSelection) {
                    setState(() {
                      ref.read(priorityProvider.notifier).update((state) => newSelection.first);
                    });
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
