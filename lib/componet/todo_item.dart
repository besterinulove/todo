import 'dart:developer';

import 'package:carbon_icons/carbon_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo/model/todo_model.dart';
import 'package:todo/provider/todo_provider.dart';

class TodoItem extends ConsumerWidget {
  final TodoModel todo;

  const TodoItem({super.key, required this.todo});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final style=Theme.of(context).textTheme.bodyMedium;
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              ref.read(todoListProvider.notifier).toggle(todo);
            },
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: todo.isCompleted
                  ? Icon(CarbonIcons.radio_button_checked, size: 24.0, color: Theme.of(context).colorScheme.secondary)
                  : Icon(
                CarbonIcons.radio_button,
                size: 24.0,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                todo.description,
                style: style?.copyWith(decoration: todo.isCompleted ? TextDecoration.lineThrough : TextDecoration.none,),
              ),
            ),
          ),
          Text(
            todo.priority.symbol,
            style: style?.copyWith(color: Colors.red),
          )
        ],
      ),
    );
  }
}
