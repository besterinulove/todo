import 'package:todo/componet/add_todo.dart';

class TodoModel {
  final Priority priority;
  final String description;

  bool isCompleted;

  TodoModel(this.priority,this.description, {this.isCompleted = false});

  TodoModel copyWith({Priority? priority,String? description, bool? isCompleted}) {
    return TodoModel(
      priority??this.priority,
      description ?? this.description,
      isCompleted: isCompleted ?? this.isCompleted,
    );
  }
}
