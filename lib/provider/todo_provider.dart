import 'dart:developer';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo/componet/add_todo.dart';
import 'package:todo/model/todo_model.dart';

final todoListProvider = NotifierProvider<TodoList, List<TodoModel>>(TodoList.new);

class TodoList extends Notifier<List<TodoModel>> {
  @override
  List<TodoModel> build() => [];

  void add(Priority priority, String description) {
    state = List.from(state)..add(TodoModel(priority, description));
  }

  void toggle(TodoModel todo) {
    todo.isCompleted = !todo.isCompleted;
    state = List.from(state);
  }

  void delete(TodoModel todo) {
    state = List.from(state)..remove(todo);
  }
}
