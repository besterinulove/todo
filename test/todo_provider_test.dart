import 'package:flutter_test/flutter_test.dart';
import 'package:riverpod/riverpod.dart';
import 'package:todo/componet/add_todo.dart';
import 'package:todo/provider/todo_provider.dart';

void main() {

  test('add', () {
    createContainer();

    final container = createContainer();
    final subscription = container.listen(todoListProvider, (previous, next) {});

    container.read(todoListProvider.notifier).add(Priority.none, '111');

    expect(
      subscription.read().length,
      1,
    );
  });

  test('toggle', () {
    createContainer();

    final container = createContainer();
    final subscription = container.listen(todoListProvider, (previous, next) {});

    container.read(todoListProvider.notifier).add(Priority.none, '111');
    container.read(todoListProvider.notifier).toggle(subscription.read()[0]);

    expect(
      subscription.read()[0].isCompleted,
      true,
    );
    container.read(todoListProvider.notifier).toggle(subscription.read()[0]);
    expect(
      subscription.read()[0].isCompleted,
      false,
    );
  });

  test('delete', () {
    createContainer();

    final container = createContainer();
    final subscription = container.listen(todoListProvider, (previous, next) {});

    container.read(todoListProvider.notifier).add(Priority.none, '111');
    container.read(todoListProvider.notifier).delete(subscription.read()[0]);

    expect(
      subscription.read().length,
      0,
    );
  });
}

ProviderContainer createContainer({
  ProviderContainer? parent,
  List<Override> overrides = const [],
  List<ProviderObserver>? observers,
}) {
  // Create a ProviderContainer, and optionally allow specifying parameters.
  final container = ProviderContainer(
    parent: parent,
    overrides: overrides,
    observers: observers,
  );

  // When the test ends, dispose the container.
  addTearDown(container.dispose);

  return container;
}
